from django.urls import path

from meteo.views import test_connections, measurements_testformat, \
    measurements_pws

urlpatterns = [
    path('measurements/pws', measurements_pws),
    path('measurements/testformat', measurements_testformat),
    path('ping', test_connections)
]
