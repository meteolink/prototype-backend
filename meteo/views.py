import logging

from django.db import connection
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from meteo.influx import client
from meteo.serializers import MeasurementSerializer, PWSMeasurementSerializer
from meteo.utils import get_client_ip

logger = logging.getLogger(__name__)


@api_view(['GET'])
def test_connections(request):
    with connection.cursor() as cursor:
        cursor.execute('SELECT version()')
        row = cursor.fetchone()

    data = {
        'influx': client.ping(),
        'postgres': row[0]
    }
    return Response(data)


@api_view(['POST'])
def measurements_wunderground(request):
    pass


@api_view(['POST'])
def measurements_testformat(request):
    serializer = MeasurementSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['GET'])
def measurements_pws(request):
    user_agent = request.META.get('HTTP_USER_AGENT')
    client_ip = get_client_ip(request)
    logger.info('user-agent: %s', user_agent)
    logger.info('client-ip: %s', client_ip)
    logger.info("data: %s", dict(request.query_params.dict()))
    logger.info('keys: %s', list(request.query_params.keys()))
    serializer = PWSMeasurementSerializer(data=request.query_params.dict())
    if serializer.is_valid():
        serializer.save(user_agent=user_agent, client_ip=client_ip)
        return Response(status=status.HTTP_204_NO_CONTENT)
    logger.warning(serializer.errors)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
