from datetime import datetime
import pytz
import logging

from rest_framework import serializers
from meteo import influx

logger = logging.getLogger(__name__)


class PWSMeasurementSerializer(serializers.Serializer):
    action = serializers.CharField()
    ID = serializers.CharField()
    PASSWORD = serializers.CharField()
    softwaretype = serializers.CharField()
    baromin = serializers.FloatField()
    dewptf = serializers.FloatField()
    humidity = serializers.FloatField()
    dateutc = serializers.DateTimeField(default_timezone=pytz.UTC)
    UV = serializers.FloatField()
    solarradiation = serializers.FloatField()
    dailyrainin = serializers.FloatField()
    winddir = serializers.FloatField(required=False)
    tempf = serializers.FloatField()
    windspeedmph = serializers.FloatField()
    windgustmph = serializers.FloatField()
    rainin = serializers.FloatField()

    def validate(self, attrs):
        unknown = set(self.initial_data.keys()) - set(self.fields.keys())
        if unknown:
            logger.warning("Unknown fields: " + ', '.join(map(repr, unknown)))
        # TODO: Test permissions
        attrs.pop('PASSWORD')
        action = attrs.pop('action')
        logger.info('action: %s', action)
        return attrs

    def create(self, validated_data):
        station_id = validated_data.pop('ID')
        timestamp = validated_data.pop('dateutc', datetime.utcnow())
        points = [
            {
                "measurement": "pws_measurement",
                "tags": {
                    "station": station_id
                },
                "time": timestamp,
                "fields": validated_data
            }
        ]
        influx.client.write_points(points)
        return validated_data


class MeasurementSerializer(serializers.Serializer):
    temp = serializers.FloatField()
    press = serializers.FloatField()
    wspeed = serializers.FloatField()
    hum = serializers.FloatField()
    rfall = serializers.FloatField()

    def create(self, validated_data):
        points = [
            {
                "measurement": "test_weather",
                "tags": {
                    "station": "fake_station"
                },
                "time": datetime.utcnow(),
                "fields": validated_data
            }
        ]
        influx.client.write_points(points)
        return validated_data
