from django.contrib import admin

from meteo.models import Profile, Station, APIKey

admin.site.register(Profile)
admin.site.register(Station)
admin.site.register(APIKey)
