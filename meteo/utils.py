import binascii
import os
import string

from django.utils.crypto import get_random_string


def new_api_key(byte_length=16):
    return binascii.b2a_hex(os.urandom(byte_length)).decode()


def new_station_id(length=8):
    allowed_chars = string.ascii_lowercase + string.digits
    return get_random_string(length, allowed_chars)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
