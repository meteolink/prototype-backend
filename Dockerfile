FROM python:3.6.5-stretch
ENV PYTHONUNBUFFERED 1

RUN apt-get -qq update && apt-get install -y -qq \
  gdal-bin \
  && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /tmp/
RUN pip3 install -r /tmp/requirements.txt

COPY . /app/
WORKDIR /app/

# Collect static files
RUN DJANGO_SETTINGS_MODULE=meteolink.settings \
    python3 manage.py collectstatic --noinput >/dev/null
